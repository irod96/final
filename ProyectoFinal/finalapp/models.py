from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Kinders(models.Model):

    numero=models.CharField(max_length=15)
    teacher=models.ForeignKey(User,on_delete=models.DO_NOTHING,default=None)
    subject =models.CharField(max_length=15)
    class_name =models.CharField(max_length=15)
    group_number=models.IntegerField()
    number_of_students_involved=models.CharField(max_length=15)

    def __str__(self):
        return self.class_name


class Padres(models.Model):

    nombre=models.CharField(max_length=15)
    hijo=models.CharField(max_length=50)

    def __str__(self):
        return self.nombre


class Persona(models.Model):

    numero=models.CharField(max_length=15)
    class_name =models.ForeignKey(Kinders,on_delete=models.DO_NOTHING,default=None)

    student_1=models.CharField(max_length=15)

    student_2=models.CharField(max_length=15)

    student_3=models.CharField(max_length=15)

    student_4=models.CharField(max_length=15)

    student_5=models.CharField(max_length=15)


    def __str__(self):
        return str(self.class_name) if self.class_name else ''

class Teachers(models.Model):

    teacher=models.ForeignKey(User,on_delete=models.DO_NOTHING,default=None)



    def __str__(self):
        return self.teacher


def get_absolute_url(self):
        return reverse('arte_edit', kwargs={'pk': self.pk})
