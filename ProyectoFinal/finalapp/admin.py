from django.contrib import admin

# Register your models here.
from .models import Kinders
admin.site.register(Kinders)

from .models import Persona
admin.site.register(Persona)

from .models import Padres
admin.site.register(Padres)
