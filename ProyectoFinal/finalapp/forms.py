from django import forms
from .models import Kinders,Persona,Teachers, Padres

class Cform(forms.ModelForm):
    class Meta:
     model=Kinders
     fields= [
      "id",
      "teacher",
      
      "class_name",
      "group_number",
      "number_of_students_involved",
     ]

class Cform2(forms.ModelForm):
     class Meta:
      model=Persona
      fields= [
      "id",
      "class_name",

      "student_1",

      "student_2",

      "student_3",

      "student_4",

      "student_5",

      ]


class Cform3(forms.ModelForm):
     class Meta:
      model=Teachers
      fields= [
       "teacher",
      ]

class Cform4(forms.ModelForm):
     class Meta:
      model=Padres
      fields= [
       "nombre",
       "hijo",
      ]
