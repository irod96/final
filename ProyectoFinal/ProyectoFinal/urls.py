"""ProyectoFinal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from finalapp import views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from finalapp import views as v
from finalapp.views import detail22

urlpatterns = [
    path('admin/', admin.site.urls),
    path("register/",v.register,name="register"),
    path('',views.index,name="index_view"),
    path('create/', views.create,name="create_view"),
    path('create2/', views.create2,name="create_view"),
    path('create3/', views.create3,name="create_view"),
    path('lista2/', views.lista2,name="lista2"),
    path('lista/', views.lista,name="lista"),
    path('lista4/', views.lista4,name="lista4"),
    #path('lista3/', views.lista4,name="lista4"),
    path('showthis/', views.showthis,name="show_this"),

    path ('detail22/<int:id>',views.detail22,name="detail223"),





    path ('detail1/<int:id>',views.detail1,name="detail_vista1"),
    path ('update/<int:id>',views.update,name="update_view"),
    path ('delete/<int:id>',views.delete,name="delete_view"),
    path ('detail2/<int:id>',views.detail2,name="detail_vista"),
    path ('update2/<int:id>',views.update2,name="update_view2"),
    path ('update3/<int:id>',views.update3,name="update_view3"),
    path ('delete2/<int:id>',views.delete2,name="delete_view2"),
    path ('delete3/<int:id>',views.delete3,name="delete_view3"),
]
